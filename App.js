/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import {AuthContext} from "./Components/Auth/authContext";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome';
import Login from './Components/Auth/Login';
import FirstScreen from './Components/Auth/FirstScreen';
import SignUp from './Components/Auth/SignUp';
import ForgotPassword from './Components/Auth/ForgotPassword';
import HomeScreen from './Components/MainScreeen/HomeScreen';
import Profile from './Components/Profile/Profile';
import Notification from './Components/Notification';
import Booking from './Components/MainScreeen/Booking';
import BookingProfile from './Components/Profile/BookingProfile';
import CardManage from './Components/Profile/CardManage';
import Friends from './Components/Profile/Friends';
import SanNguyenDu from './Components/BookingSan/SanNguyenDu';
import SanChaoLua from './Components/BookingSan/SanChaoLua';
import SplashScreen from "./Components/SplashScreen";
import SanThanhNien from "./Components/BookingSan/SanThanhNien";
import SanHoangPhu from "./Components/BookingSan/SanHoangPhu";
import SanRachDua from "./Components/BookingSan/SanRachDua";
import SanLamSon from "./Components/BookingSan/SanLamSon";
import MainNews from "./Components/News/MainNews";
import News1 from "./Components/News/News1";
import News2 from "./Components/News/News2";
import News3 from "./Components/News/News3";
import News4 from "./Components/News/News4";
import News5 from "./Components/News/News5";
console.disableYellowBox = true;

const StackHome = createStackNavigator()

function HomeStack({navigation, route}) {
    if (route.state && route.state.index > 0){
        navigation.setOptions({tabBarVisible: false})
    }else {
        navigation.setOptions({tabBarVisible: true})
    }
    return(
        <StackHome.Navigator initialRouteName="Home" headerMode="none">
            <StackHome.Screen name="Home" component={HomeScreen}/>
            <StackHome.Screen name="Notification" component={Notification}/>
            <StackHome.Screen name="MainNews" component={NewsStack}/>
        </StackHome.Navigator>
    )
}

const StackProfile = createStackNavigator()

function ProfileStack({navigation, route}){
    if (route.state && route.state.index > 0){
        navigation.setOptions({tabBarVisible: false})
    }else {
        navigation.setOptions({tabBarVisible: true})
    }
    return(
        <StackProfile.Navigator initialRouteName="Profile" headerMode="none">
            <StackProfile.Screen name="Profile" component={Profile}/>
        </StackProfile.Navigator>
    )
}


const StackBooking = createStackNavigator()

function BookingStack({navigation, route}){
    if (route.state && route.state.index > 0){
        navigation.setOptions({tabBarVisible: false})
    }else {
        navigation.setOptions({tabBarVisible: true})
    }
    return(
        <StackBooking.Navigator initialRouteName="Booking" headerMode="none">
            <StackBooking.Screen name="Booking" component={Booking}/>
            <StackBooking.Screen name="SanNguyenDu" component={SanNguyenDu}/>
            <StackBooking.Screen name="SanThanhNien" component={SanThanhNien}/>
            <StackBooking.Screen name="SanHoangPhu" component={SanHoangPhu}/>
            <StackBooking.Screen name="SanRachDua" component={SanRachDua}/>
            <StackBooking.Screen name="SanLamSon" component={SanLamSon}/>
            <StackBooking.Screen name="SanChaoLua" component={SanChaoLua}/>
        </StackBooking.Navigator>
    )
}

const StackNews = createStackNavigator()

function NewsStack({navigation, route}) {
    if (route.state && route.state.index > 0){
        navigation.setOptions({tabBarVisible: false})
    }else {
        navigation.setOptions({tabBarVisible: true})
    }
    return(
        <StackNews.Navigator headerMode="none">
            <StackNews.Screen name="MainNews" component={MainNews}/>
            <StackNews.Screen name="News1" component={News1}/>
            <StackNews.Screen name="News2" component={News2}/>
            <StackNews.Screen name="News3" component={News3}/>
            <StackNews.Screen name="News4" component={News4}/>
            <StackNews.Screen name="News5" component={News5}/>
        </StackNews.Navigator>
    )
}




const Tab = createBottomTabNavigator();

function TabBottomScreen() {
    return(
        <Tab.Navigator activeColor="white" style={{ backgroundColor: 'white' }}>
            <Tab.Screen name="Home" component={HomeStack} options={{
                tabBarLabel: 'Home',
                tabBarIcon: ({ color}) => (
                    <Icon name="home" color={color} size={26}/>
                ),
            }}
            />
            <Tab.Screen name="Đặt sân" component={BookingStack} options={{
                tabBarLabel: 'Sân bóng',
                tabBarIcon: ({ color}) => (
                    <Icon name="book" color={color} size={26}/>
                ),
            }}
            />
            <Tab.Screen name="Thông báo" component={Notification} options={{
                tabBarLabel: 'Thông báo',
                tabBarIcon: ({ color}) => (
                    <Icon name="bell" color={color} size={26}/>
                ),
            }}
            />
            <Tab.Screen name="Profile" component={ProfileStack} options={{
                tabBarLabel: 'Profile',
                tabBarIcon: ({ color}) => (
                    <Icon name="user" color={color} size={26}/>
                ),
            }}
            />
        </Tab.Navigator>
    )
}

const AuthStack = createStackNavigator()

function AuthScreen() {
    return(
        <AuthStack.Navigator headerMode='none' initialRouteName="Login">
            <AuthStack.Screen name="Login" component={Login}/>
            <AuthStack.Screen name="SignUp" component={SignUp}/>
        </AuthStack.Navigator>
    )
}




function App(){
    const [isLoading, setIsLoading] = React.useState(true);
    const [Token, setToken] = React.useState(null);
    const authContext = React.useMemo(() => {
        return {
            signIn: () => {
                setIsLoading(false);
                setToken('1');
            },
            Register: () => {
                setIsLoading(false);
                setToken(null);
            },
            Logout: () => {
                setIsLoading(false);
                setToken(null);
            }
        }
    }, []);

    React.useEffect( () => {
        setTimeout(() => {
            setIsLoading(false);
        }, 2000);
    }, []);

  return (
      <AuthContext.Provider value={authContext}>
          <NavigationContainer>
              {  isLoading ? SplashScreen() :
                  Token ? TabBottomScreen() : AuthScreen()
              }
          </NavigationContainer>
      </AuthContext.Provider>

  );
};



export default App;
