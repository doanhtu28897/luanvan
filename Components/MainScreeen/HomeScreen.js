import React from 'react';
import {View, Text, Image, ScrollView, StyleSheet,Dimensions,TouchableOpacity} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import {Button,Form, Item, Input, Body, Grid, Col, Row} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import Notification from '../Notification';
import Booking from './Booking';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');
const SLIDE_WIDTH = Math.round(viewportWidth / 1.5);
const ITEM_HORIZONTAL_MARGIN = 5;
const itemWidth = SLIDE_WIDTH + ITEM_HORIZONTAL_MARGIN * 3;
const sliderWidth = viewportWidth;

const TopSan = [
    {
        image: 'https://placevietnam.com/img/600x337/san_pham/san-bong-da-nguyen-du.JPG',
        title: 'Sân Nguyễn Du',
        ranking: '4.1',
    },
    {
        image: 'https://placevietnam.com/img/420x240/san_pham/san-bong-da-nha-van-hoa-thanh-nien.JPG',
        title: 'Sân Văn Hoá Thanh Niên',
        ranking: '4.5'
    },
    {
        image: 'https://placevietnam.com/img/600x337/san_pham/san-bong-da-hoang-phu.jpg',
        title: 'Sân Hoàng Phú',
        ranking: '4.2'
    },
    {
        image: 'https://placevietnam.com/img/420x240/san_pham/san-bong-da-co-nhan-tao-tt-tay-bac.jpg',
        title: 'Sân Rạch Dừa',
        ranking: '4.0'
    },
    {
        image: 'https://cdn.vatgia.vn/pictures/thumb/w500/2013/08/hje1375408311.jpg',
        title: 'Sân Lam Sơn',
        ranking: '4.3'
    },
]

const TinTuc = [
    {
        image: 'https://cdn.24h.com.vn/upload/4-2020/images/2020-10-06/medium/ipiccy-collage-1601995448-269-width640height480.jpg',
        title: 'Đế chế Ronaldo ở Juventus tiến gần ngày tàn...',
        link: 'https://www.24h.com.vn/bong-da/de-che-ronaldo-o-juventus-tien-gan-ngay-tan-bom-tan-chiesa-hay-ra-sao-c48a1188464.html'
    },
    {
        image: 'https://cdn.24h.com.vn/upload/4-2020/images/2020-10-07/medium/7-640-1602045672-991-width640height480.jpg',
        title: 'Barcelona đón tin dữ: Thua lỗ quá nặng, Messi...',
        link: 'https://www.24h.com.vn/bong-da/barcelona-don-tin-du-thua-lo-qua-nang-messi-co-quyen-ra-di-ngay-lap-tuc-c48a1188598.html'
    },
    {
        image: 'https://cdn.24h.com.vn/upload/4-2020/images/2020-10-07/120775239_3346754632046842_21677231894216866_n-1602040498-472-width660height464.jpg',
        title: 'Đội Hà Nam bỏ cuộc phản đối trọng tài gây sốc...',
        link: 'https://www.24h.com.vn/bong-da/doi-ha-nam-bo-cuoc-phan-doi-trong-tai-gay-soc-hlv-cuong-bi-vff-cam-5-nam-c48a1188556.html'
    },
    {
        image: 'https://cdn.24h.com.vn/upload/4-2020/images/2020-10-07/1-660-1602032829-132-width660height440.jpg',
        title: 'Maguire ngăn Rashford khiếu nại vụ Martial thẻ đỏ:...',
        link: 'https://www.24h.com.vn/bong-da/maguire-ngan-rashford-khieu-nai-vu-martial-the-do-fan-doi-tuoc-bang-thu-quan-mu-c48a1188510.html'
    },
    {
        image: 'https://cdn.24h.com.vn/upload/4-2020/images/2020-10-06/unnamed-660-1601917439-864-width660height440.jpg',
        title: 'Kane & Son Heung Min phong độ hủy diệt:...',
        link: 'https://www.24h.com.vn/bong-da/kane-son-heung-min-phong-do-huy-diet-tottenham-them-bale-la-du-vo-dich-c48a1188179.html'
    },
]

const SanGiamGia = [
    {
        image: 'https://placevietnam.com/img/420x240/san_pham/san-bong-da-nha-van-hoa-thanh-nien.JPG',
        title: 'Sân Văn Hoá Thanh Niên',
        ranking: '4.5'
    },
    {
        image: 'https://placevietnam.com/img/600x337/san_pham/san-bong-da-hoang-phu.jpg',
        title: 'Sân Hoàng Phú',
        ranking: '4.2'
    },
    {
        image: 'https://placevietnam.com/img/420x240/san_pham/san-bong-da-co-nhan-tao-tt-tay-bac.jpg',
        title: 'Sân Tây Bắc',
        ranking: '4.1'
    },
    {
        image: 'https://cdn.vatgia.vn/pictures/thumb/w500/2013/08/hje1375408311.jpg',
        title: 'Sân Lam Sơn',
        ranking: '4.3'
    },
    {
        image: 'https://placevietnam.com/img/600x337/san_pham/san-bong-da-mini-chao-lua-scsc.JPG',
        title: 'Sân Chảo Lửa',
        ranking: '4.0'
    },
]


function HomeScreen({navigation}){

    function SanBong(value1) {
        return(
            <TouchableOpacity style={{marginTop: 20, width: '100%', paddingRight: 20, paddingLeft: 10}}>
                <Image source={{uri: value1.item.image}} style={{width: '100%',height: 150, borderRadius: 10}}/>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 5}}>
                    <Text style={{fontSize: 15, fontWeight: 'bold'}}>{value1.item.title}</Text>
                    <View style={{flexDirection: 'row',alignItems: 'center'}}>
                        <Text style={{marginRight: 5}}>{value1.item.ranking}</Text>
                        <Icon name='star' style={{fontSize: 15, color: 'red'}}/>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    function News(value2) {
        return(
            <TouchableOpacity style={{marginTop: 20, width: '100%', paddingRight: 20, paddingLeft: 10}}>
                <Image source={{uri: value2.item.image}} style={{width: '100%',height: 150, borderRadius: 10}}/>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 5}}>
                    <Text style={{fontSize: 15, fontWeight: 'bold'}}>{value2.item.title}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    function Discount(value3) {
        return(
            <TouchableOpacity style={{marginTop: 20, width: '100%', paddingRight: 20, paddingLeft: 10}}>
                <Image source={{uri: value3.item.image}} style={{width: '100%',height: 150, borderRadius: 10}}/>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 5}}>
                    <Text style={{fontSize: 15, fontWeight: 'bold'}}>{value3.item.title}</Text>
                    <View style={{flexDirection: 'row',alignItems: 'center'}}>
                        <Text style={{marginRight: 5}}>{value3.item.ranking}</Text>
                        <Icon name='star' style={{fontSize: 15, color: 'red'}}/>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    return(
        <ScrollView style={{backgroundColor: 'lightgrey'}}>
            <View style={{flexDirection: 'row', backgroundColor: 'lightblue', height: 70, alignItems: 'center'}}>
                <Image source={{uri: 'https://banner2.cleanpng.com/20180920/yko/kisspng-computer-icons-portable-network-graphics-avatar-ic-5ba3c66df14d32.3051789815374598219884.jpg'}} style={{height: 50, width: 50, borderRadius: 50}}/>
                <Body>
                    <Item rounded style={{backgroundColor: 'white', marginRight: 10, marginLeft: 10}}>
                        <Input placeholder='Tìm kiếm'/>
                        <Icon name='search' style={{fontSize: 20, marginRight: 10}} />
                    </Item>
                </Body>
                <Icon name='bell' style={{fontSize: 20, marginRight: 5, color: 'white'}} onPress={() => navigation.navigate('Notification')}/>
            </View>
            <Grid style={{backgroundColor: 'white', height: 70, marginTop: 10}}>
                <Col style={styles.grid}>
                    <Image source={{uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/Circle-icons-news.svg/1200px-Circle-icons-news.svg.png'}} style={{height: 40, width: 40}}/>
                    <Text>Tin tức</Text>
                </Col>
                <Col style={styles.grid}>
                    <Image source={{uri: 'https://www.iconfinder.com/data/icons/football-18/512/football-19-512.png'}} style={{height: 40, width: 40}}/>
                    <Text>Giải đấu</Text>
                </Col>
                <Col style={styles.grid}>
                    <Image source={{uri: 'https://www.iconfinder.com/data/icons/game-design-butterscotch-vol-1/256/Leader-512.png'}} style={{height: 40, width: 40}}/>
                    <Text>Bảng XH</Text>
                </Col>
            </Grid>
            <View style={{backgroundColor: 'white', marginTop: 10}}>
                <View style={{backgroundColor: 'white', marginTop: 10, flexDirection: 'row', justifyContent: 'space-between'}}>
                    <Text style={{marginLeft: 10, color: 'blue', fontSize: 20, fontWeight: 'bold'}}>Top Sân</Text>
                    <Text style={{textDecorationLine: 'underline',color: 'deepskyblue', marginRight: 20, alignSelf: 'center'}} onPress ={() => navigation.navigate('#')}>Xem tất cả</Text>
                </View>
                <Carousel
                    ref={(c) => { _carousel = c; }}
                    data={TopSan}
                    renderItem={SanBong}
                    sliderWidth={sliderWidth}
                    itemWidth={itemWidth}
                    layout={'default'}
                    inactiveSlideScale={1}
                    inactiveSlideOpacity={1}
                    activeSlideAlignment={'start'}
                />
                <View style={{marginTop: 10, flexDirection: 'row', justifyContent: 'space-between'}}>
                    <Text style={{marginLeft: 10, color: 'blue', fontSize: 20, fontWeight: 'bold'}}>Bảng Tin</Text>
                    <Text style={{textDecorationLine: 'underline',color: 'deepskyblue', marginRight: 20, alignSelf: 'center'}} onPress = {() => navigation.navigate('MainNews')}>Xem tất cả</Text>
                </View>
                <Carousel
                    ref={(c) => { _carousel = c; }}
                    data={TinTuc}
                    renderItem={News}
                    sliderWidth={sliderWidth}
                    itemWidth={itemWidth}
                    layout={'default'}
                    inactiveSlideScale={1}
                    inactiveSlideOpacity={1}
                    activeSlideAlignment={'start'}
                />
                <View style={{marginTop: 10, flexDirection: 'row', justifyContent: 'space-between'}}>
                    <Text style={{marginLeft: 10, color: 'blue', fontSize: 20, fontWeight: 'bold'}}>Sân Giảm Giá</Text>
                    <Text style={{textDecorationLine: 'underline',color: 'deepskyblue', marginRight: 20, alignSelf: 'center'}} onPress ={() => navigation.navigate('#')}>Xem tất cả</Text>
                </View>
                <Carousel
                    ref={(c) => { _carousel = c; }}
                    data={SanGiamGia}
                    renderItem={Discount}
                    sliderWidth={sliderWidth}
                    itemWidth={itemWidth}
                    layout={'default'}
                    inactiveSlideScale={1}
                    inactiveSlideOpacity={1}
                    activeSlideAlignment={'start'}
                />
            </View>

        </ScrollView>
    );
}
export default HomeScreen;

const styles = StyleSheet.create({
    grid: {
        alignItems: 'center',
        alignSelf: 'center'
    },
})
