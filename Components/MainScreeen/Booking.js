import React from 'react';
import {View, Text, ScrollView, ImageBackground, Image, TouchableOpacity} from 'react-native';
import {Button,Form, Item, Input, Body, Grid, Col, Row,Right} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';




function Booking({navigation}) {
    return(
        <ScrollView>
            <ImageBackground source={{uri: 'https://i.pinimg.com/736x/e9/f4/bc/e9f4bc830e15888a54f06f1382b413d9.jpg'}} style={{width: '100%', height: 200}}>
                <View style={{flexDirection: 'row'}}>
                    <Body>
                        <Item rounded style={{backgroundColor: 'white', marginLeft: 10, marginTop: 10, height: 40, borderRadius: 5}}>
                            <Input placeholder='Tìm kiếm'/>
                            <Icon name='search' style={{fontSize: 20, marginRight: 10}}/>
                        </Item>
                    </Body>
                    <Icon name='calendar'/>
                </View>
            </ImageBackground>
            <View style={{marginTop: 10, marginLeft: 10}}>
                <Text style={{fontSize: 20, fontWeight: 'bold'}}>Đề xuất dành cho bạn</Text>
            </View>
            <TouchableOpacity  onPress = {() => navigation.navigate('SanNguyenDu')}>
                <Image source={{uri: 'https://placevietnam.com/img/600x337/san_pham/san-bong-da-nguyen-du.JPG'}} style={{height: 200, marginLeft: 10, marginRight: 10, borderRadius: 10}}/>
                <View style={{position: 'absolute', marginLeft: 20, marginTop: 80}}>
                    <Text style={{color: 'white', fontWeight: 'bold', marginTop: 5}}>06:00 - 22:00</Text>
                    <Text style={{color: 'red', fontWeight: 'bold', marginTop: 5}}>200.000 ₫ - 500.000 ₫</Text>
                    <Text style={{color: 'white', fontWeight: 'bold', fontSize: 20, marginTop: 5}}>Sân bóng đá Nguyễn Du</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity style={{marginTop: 20}}  onPress = {() => navigation.navigate('SanThanhNien')}>
                <Image source={{uri: 'https://placevietnam.com/img/420x240/san_pham/san-bong-da-nha-van-hoa-thanh-nien.JPG'}} style={{height: 200, marginLeft: 10, marginRight: 10, borderRadius: 10}}/>
                <View style={{position: 'absolute', marginLeft: 20, marginTop: 80}}>
                    <Text style={{color: 'white', fontWeight: 'bold', marginTop: 5}}>07:00 - 22:00</Text>
                    <Text style={{color: 'red', fontWeight: 'bold', marginTop: 5}}>300.000 ₫ - 800.000 ₫</Text>
                    <Text style={{color: 'white', fontWeight: 'bold', fontSize: 20, marginTop: 5}}>Sân bóng đá Nhà Văn hóa Thanh Niên</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity style={{marginTop: 20}}  onPress = {() => navigation.navigate('SanHoangPhu')}>
                <Image source={{uri: 'https://placevietnam.com/img/600x337/san_pham/san-bong-da-hoang-phu.jpg'}} style={{height: 200, marginLeft: 10, marginRight: 10, borderRadius: 10}}/>
                <View style={{position: 'absolute', marginLeft: 20, marginTop: 80}}>
                    <Text style={{color: 'white', fontWeight: 'bold', marginTop: 5}}>06:00 - 22:00</Text>
                    <Text style={{color: 'red', fontWeight: 'bold', marginTop: 5}}>120.000 ₫ - 300.000 ₫</Text>
                    <Text style={{color: 'white', fontWeight: 'bold', fontSize: 20, marginTop: 5}}>Sân bóng đá Hoàng Phú</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity style={{marginTop: 20}}  onPress = {() => navigation.navigate('SanRachDua')}>
                <Image source={{uri: 'https://placevietnam.com/img/420x240/san_pham/san-bong-da-co-nhan-tao-tt-tay-bac.jpg'}} style={{height: 200, marginLeft: 10, marginRight: 10, borderRadius: 10}}/>
                <View style={{position: 'absolute', marginLeft: 20, marginTop: 80}}>
                    <Text style={{color: 'white', fontWeight: 'bold', marginTop: 5}}>06:00 - 22:00</Text>
                    <Text style={{color: 'red', fontWeight: 'bold', marginTop: 5}}>300.000 ₫ - 800.000 ₫</Text>
                    <Text style={{color: 'white', fontWeight: 'bold', fontSize: 20, marginTop: 5}}>Sân bóng đá Khu Du Lịch Rạch Dừa</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity style={{marginTop: 20}}  onPress = {() => navigation.navigate('SanLamSon')}>
                <Image source={{uri: 'https://cdn.vatgia.vn/pictures/thumb/w500/2013/08/hje1375408311.jpg'}} style={{height: 200, marginLeft: 10, marginRight: 10, borderRadius: 10}}/>
                <View style={{position: 'absolute', marginLeft: 20, marginTop: 80}}>
                    <Text style={{color: 'white', fontWeight: 'bold', marginTop: 5}}>06:00 - 22:00</Text>
                    <Text style={{color: 'red', fontWeight: 'bold', marginTop: 5}}>200.000 ₫ - 360.000 ₫</Text>
                    <Text style={{color: 'white', fontWeight: 'bold', fontSize: 20, marginTop: 5}}>Sân bóng đá Trung tâm bóng đá Lam Sơn</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity style={{marginTop: 20, marginBottom: 10}}  onPress = {() => navigation.navigate('SanChaoLua')}>
                <Image source={{uri: 'https://placevietnam.com/img/600x337/san_pham/san-bong-da-mini-chao-lua-scsc.JPG'}} style={{height: 200, marginLeft: 10, marginRight: 10, borderRadius: 10}}/>
                <View style={{position: 'absolute', marginLeft: 20, marginTop: 80}}>
                    <Text style={{color: 'white', fontWeight: 'bold', marginTop: 5}}>06:00 - 22:00</Text>
                    <Text style={{color: 'red', fontWeight: 'bold', marginTop: 5}}>100.000 ₫ - 300.000 ₫</Text>
                    <Text style={{color: 'white', fontWeight: 'bold', fontSize: 20, marginTop: 5}}>ân bóng đá mini Chảo Lửa SCSC</Text>
                </View>
            </TouchableOpacity>
        </ScrollView>
    )
}

export default Booking;
