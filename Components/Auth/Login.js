import React from 'react';
import {View, Text, Image, ImageBackground, TextInput} from 'react-native';
import {Button,Row, Input, Item} from 'native-base';
import Icon from "react-native-vector-icons/FontAwesome";
import * as Animatable from 'react-native-animatable';
import {AuthContext} from "./authContext";
function Login({navigation}) {
    const {signIn} = React.useContext(AuthContext)
    return(
        <View style={{backgroundColor: 'lightblue', height: '100%'}}>
            <Row/>
            <Animatable.Image
                animation = "bounceIn"
                source={{uri: 'https://i.pinimg.com/originals/1c/b5/d6/1cb5d6bed0d048db2d8f0a92097b0e9b.png'}}
                style={{width: 100, height: 100, alignSelf: 'center'}}/>
                <View style={{backgroundColor: 'white', margin: 20, borderRadius: 20}}>
                    <View style={{flexDirection: 'row', borderBottomWidth: 1, margin: 5}}>
                        <Icon name="user-o"
                              color="#05375a"
                              size={20}
                              style={{alignSelf: 'center', marginRight: 10}}/>
                        <TextInput
                            placeholder="Tài khoản"
                            autoCapitalize="none"
                        />
                    </View>
                    <View style={{flexDirection: 'row', borderBottomWidth: 1, margin: 5, marginBottom: 10}}>
                        <Icon name="lock"
                              color="#05375a"
                              size={20}
                              style={{alignSelf: 'center', marginRight: 10}}/>
                        <TextInput
                            placeholder="Mật khẩu"
                            autoCapitalize="none"
                            secureTextEntry={true}/>
                    </View>
                </View>
            <Button block style={{margin: 20, borderRadius: 10}} onPress={() => signIn()}>
                <Text style={{fontWeight: 'bold', color: 'white'}}>Đăng nhập</Text>
            </Button>
            <Button block style={{borderRadius: 10, marginLeft: 20, marginRight: 20, backgroundColor: 'red'}}>
                <Text style={{fontWeight: 'bold', color: 'white'}} onPress={() => navigation.navigate('SignUp')}>Đăng Ký</Text>
            </Button>
            <Row/>
        </View>
    );
}
export default Login;
