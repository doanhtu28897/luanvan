import React from 'react';
import {View, Text, ImageBackground, Image} from 'react-native';
import {Button, Form, Input, Item, Row} from 'native-base';
function SignUp({navigation}) {
    return(
        <ImageBackground source={{uri: 'https://grownandflown.com/wp-content/uploads/2018/11/shutterstock_786447454-700x370.jpg'}} style={{height: '100%'}}>
            <Row/>
            <Image source={{uri: 'https://i.pinimg.com/originals/1c/b5/d6/1cb5d6bed0d048db2d8f0a92097b0e9b.png'}} style={{width: 100, height: 100, alignSelf: 'center'}}/>
            <View>
                <Form style={{marginBottom: 30, marginLeft: 20, marginRight: 20, marginTop: 20}}>
                    <Item rounded style={{backgroundColor: 'white'}}>
                        <Input placeholder='UserName'/>
                    </Item>
                    <Item rounded style={{backgroundColor: 'white', marginTop: 15}}>
                        <Input placeholder='PassWord'/>
                    </Item>
                    <Item rounded style={{backgroundColor: 'white', marginTop: 15}}>
                        <Input placeholder='Phone'/>
                    </Item>
                    <Item rounded style={{backgroundColor: 'white', marginTop: 15}}>
                        <Input placeholder='Email'/>
                    </Item>
                </Form>
            </View>
            <Button block style={{marginLeft: 20, marginRight: 20, height: 50, borderRadius: 10, marginTop: 15}}>
                <Text style={{color: 'white', fontWeight: 'bold', fontSize: 20}}>SignUp</Text>
            </Button>
            <Row/>
        </ImageBackground>
    );
}
export default SignUp;
