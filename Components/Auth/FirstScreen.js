import React from 'react';
import {View, Text, Image, ImageBackground} from 'react-native';
import {Button,Row} from 'native-base';
function FirstScreen() {
    return(
        <ImageBackground source={{uri: 'https://grownandflown.com/wp-content/uploads/2018/11/shutterstock_786447454-700x370.jpg'}} style={{height: '100%'}}>
            <Row/>
            <Image source={{uri: 'https://i.pinimg.com/originals/1c/b5/d6/1cb5d6bed0d048db2d8f0a92097b0e9b.png'}} style={{width: 100, height: 100, alignSelf: 'center'}}/>
            <Row/>
            <View>
                <Button block style={{marginRight: 15, marginLeft: 15, borderRadius: 15, height: 50, backgroundColor: 'red'}}>
                    <Text style={{color: 'white', fontWeight: 'bold', fontSize: 20}}>Login</Text>
                </Button>
                <Button block style={{marginRight: 15, marginLeft: 15, borderRadius: 15, marginTop: 20, height: 50, backgroundColor: 'aqua'}}>
                    <Text style={{color: 'white', fontWeight: 'bold', fontSize: 20}}>SignUp</Text>
                </Button>
            </View>
            <Row/>
        </ImageBackground>
    )
}

export default FirstScreen;
