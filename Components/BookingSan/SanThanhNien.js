import React, {useState} from 'react';
import {View, Text, Image, TouchableOpacity, ScrollView, StyleSheet} from 'react-native';
import {Row, Col, Right} from 'native-base'
import Icon from 'react-native-vector-icons/FontAwesome';
import CalendarStrip from 'react-native-calendar-strip';
import moment from 'moment';

const KhungGio = [
    {
        time: '06:00' ,
        cost: '200.000 ₫',
        gift: 'https://supership.vn/wp-content/uploads/2019/02/gift-box-icon-1.png',
        gifttitle: 'Quà tặng'
    },
    {
        time: '07:00' ,
        cost: '200.000 ₫',
        gift: 'https://supership.vn/wp-content/uploads/2019/02/gift-box-icon-1.png',
        gifttitle: 'Quà tặng'
    },
    {
        time: '08:00' ,
        cost: '200.000 ₫',
    },
    {
        time: '09:00' ,
        cost: '200.000 ₫',
    },
    {
        time: '10:00' ,
        cost: '200.000 ₫',

        gift: 'https://supership.vn/wp-content/uploads/2019/02/gift-box-icon-1.png',
        gifttitle: 'Quà tặng'
    },{
        time: '11:00' ,
        cost: '200.000 ₫',
    },
    {
        time: '12:00' ,
        cost: '200.000 ₫',
    },
    {
        time: '13:00' ,
        cost: '250.000 ₫',
        gift: 'https://supership.vn/wp-content/uploads/2019/02/gift-box-icon-1.png',
        gifttitle: 'Quà tặng'
    },
    {
        time: '14:00' ,
        cost: '250.000 ₫',
        gift: 'https://supership.vn/wp-content/uploads/2019/02/gift-box-icon-1.png',
        gifttitle: 'Quà tặng'
    },
    {
        time: '15:00' ,
        cost: '300.000 ₫',
    },
    {
        time: '16:00' ,
        cost: '300.000 ₫',
        gift: 'https://supership.vn/wp-content/uploads/2019/02/gift-box-icon-1.png',
        gifttitle: 'Quà tặng'
    },
    {
        time: '17:00' ,
        cost: '300.000 ₫',
    },
    {
        time: '18:00' ,
        cost: '300.000 ₫',
    },
    {
        time: '19:00' ,
        cost: '400.000 ₫',
    },
    {
        time: '20:00' ,
        cost: '400.000 ₫',
    },
    {
        time: '21:00' ,
        cost: '400.000 ₫',
    }
]

let item = KhungGio.map((item, index, ) => {
    return(
        <TouchableOpacity style={{
            borderWidth: 0.5,
            height: 70,
            justifyContent: 'center',
            borderColor: 'red'
        }}>
            <View style={{margin: 10,
                justifyContent: 'space-between',
                flexDirection: 'row'
            }}>
                <View>
                    <Text style={{fontSize: 20, fontWeight: 'bold'}}>{item.time}</Text>
                    <Text style={{fontSize: 15, color: 'red'}}>{item.cost}</Text>
                </View>
                <Right>
                    <Row>
                        <Text style={{marginRight: 10}}>{item.gifttitle}</Text>
                        <Image source={{uri: item.gift}} style={{height: 20, width: 20}}/>
                    </Row>
                </Right>
            </View>
        </TouchableOpacity>
    )
})

function SanThanhNien({navigation}) {
    let  [customDatesStyles] = useState([]);
    let startDate = moment(); // today
    let date = startDate.clone().add(0, 'days');

    customDatesStyles.push({
        startDate: date, // Single date since no endDate provided
        dateNameStyle: {color: '#000'},
        dateNumberStyle: {color: '#000'},
    });

    const onDateSelected = date => {
        let datecurrent = date.clone().add(0, 'days');
        customDatesStyles = [];

        customDatesStyles.push({
            startDate: datecurrent, // Single date since no endDate provided
            dateNameStyle: {color: '#fff'},
            dateNumberStyle: {color: '#fff'},
            // Random color...
            dateContainerStyle: {backgroundColor:'#00a6f2', color: '#fff'},
        });


    };

    return(
        <ScrollView>
            <View>
                <Image source={{uri: 'https://placevietnam.com/img/600x337/san_pham/san-bong-da-nguyen-du.JPG'}} style={{width: '100%', height: 200}}/>
                <View style={{position: 'absolute'}}>
                    <Icon name='arrow-left' style={{color: 'white', fontSize: 20, margin: 20}} onPress={() => navigation.goBack()}/>
                    <View style={{margin: 30}}>
                        <Text style={{fontSize: 20, fontWeight: 'bold', color: 'white'}}>Sân bóng đá Nguyễn Du</Text>
                        <Text style={{fontWeight: 'bold', color: 'white', marginTop: 5}}>116 Nguyễn Du, Bến Thành, Quận 1</Text>
                        <Row>
                            <Icon name='money' style={{color: 'white', marginRight: 10, fontSize: 15, marginTop: 7}}/>
                            <Text style={{color: 'white', marginTop: 5}}>Thanh toán tại sân</Text>
                        </Row>
                    </View>
                </View>
            </View>
            <View style={{margin: 20}}>
                <View>
                    <CalendarStrip
                        scrollable
                        iconContainer={{flex: 0.1}}
                        calendarAnimation={{type: 'sequence', duration: 30}}
                        // daySelectionAnimation={{type: 'background', duration: 300, borderWidth: 1, highlightColor: '#000'}}
                        calendarHeaderStyle={{color: '#000', marginBottom: 20}}
                        dateNumberStyle={{color: '#000'}}
                        dateNameStyle={{color: '#000'}}
                        highlightDateNumberStyle={{color: 'blue'}}
                        highlightDateNameStyle={{color: 'blue'}}
                        style={{paddingBottom: 20, height: 60}}
                        customDatesStyles={customDatesStyles}
                        onDateSelected={onDateSelected}
                        useIsoWeekday={false}
                    />
                </View>
                <Text style={{fontSize: 20, fontWeight: 'bold', color: 'blue'}}>Khung Giờ</Text>
            </View>
            <View>
                {item}
            </View>
        </ScrollView>
    )
}

export default SanThanhNien;

