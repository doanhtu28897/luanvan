import React from 'react';
import {View, Text} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

function Friends({navigation}) {
    return(
        <View style={{backgroundColor: 'lightblue', flexDirection: 'row', justifyContent: 'space-between', height: 70, alignItems: 'center'}}>
            <Icon name='arrow-left' style={{color: 'white', fontWeight: 'bold', fontSize: 20, marginLeft: 10}}/>
            <Text style={{color: 'white', fontWeight: 'bold', fontSize: 20}}>Bạn bè</Text>
            <Text/>
        </View>
    )
}
export default Friends;
