import React from  'react';
import {View, Text, ScrollView, Image, TouchableOpacity, StyleSheet} from 'react-native';
import {Body, Input, Item, Right} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';

function Profile({navigation}) {
    return(
        <ScrollView style={{backgroundColor: 'lightgrey'}}>
            <View style={{flexDirection: 'row', backgroundColor: 'lightblue', height: 70, alignItems: 'center'}}>
                <Image source={{uri: 'https://banner2.cleanpng.com/20180920/yko/kisspng-computer-icons-portable-network-graphics-avatar-ic-5ba3c66df14d32.3051789815374598219884.jpg'}} style={{height: 50, width: 50, borderRadius: 50}}/>
                <View style={{marginLeft: 10}}>
                    <Text>Xin chào</Text>
                    <Text>Tú Đỗ</Text>
                </View>
                <Right>
                    <Icon name='chevron-right' style={{fontSize: 20, marginRight: 20, color: 'white'}}/>
                </Right>
            </View>
            <View style={{backgroundColor: 'white', marginTop: 10}}>
                <TouchableOpacity style={{flexDirection: 'row', alignItems: 'center', marginLeft: 10, marginTop: 10}}>
                    <Image source={{uri: 'https://www.pinclipart.com/picdir/middle/565-5658001_booking-icon-font-awesome-clipart.png'}} style={{height: 50, width: 50, borderRadius: 15}}/>
                    <Text style={{fontSize: 20, marginLeft: 10}}>Đặt sân</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{flexDirection: 'row', alignItems: 'center', marginLeft: 10, marginTop: 10}}>
                    <Image source={{uri: 'https://www.iconfinder.com/data/icons/business-flat-18/512/business_information__management__card_-512.png'}} style={{height: 50, width: 50, borderRadius: 15}}/>
                    <Text style={{fontSize: 20, marginLeft: 10}}>Quản lý thẻ</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{flexDirection: 'row', alignItems: 'center', marginLeft: 10, marginTop: 10}}>
                    <Image source={{uri: 'https://c7.uihere.com/files/690/216/39/computer-icons-friends-icon.jpg'}} style={{height: 50, width: 50, borderRadius: 25}}/>
                    <Text style={{fontSize: 20, marginLeft: 10}}>Bạn bè</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{flexDirection: 'row', alignItems: 'center', marginLeft: 10, marginTop: 10}}>
                    <Image source={{uri:'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSbTVyG9PExfhJ5uZ0GZnkXykHRKiXQ2gEaoQ&usqp=CAU'}} style={{height: 50, width: 50, borderRadius: 15}}/>
                    <Text style={{fontSize: 20, marginLeft: 10}}>Nhóm</Text>
                </TouchableOpacity>
            </View>
            <View>
                <TouchableOpacity style={{flexDirection: 'row', alignItems: 'center', marginLeft: 10, marginTop: 10}}>
                    <Image source={{uri:'https://www.iconfinder.com/data/icons/vector-icons-for-mobile-apps-2/512/Settings_black-512.png'}} style={{height: 50, width: 50, borderRadius: 15}}/>
                    <Text style={{fontSize: 20, marginLeft: 10}}>Cài đặt</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{flexDirection: 'row', alignItems: 'center', marginLeft: 10, marginTop: 10}}>
                    <Image source={{uri:'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTkAenNRsJ9TzwFq8vEkAM1vKgIY_v1cgS8Hw&usqp=CAU'}} style={{height: 50, width: 50, borderRadius: 15}}/>
                    <Text style={{fontSize: 20, marginLeft: 10}}>Đăng xuất</Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
    )
}
export default Profile;
