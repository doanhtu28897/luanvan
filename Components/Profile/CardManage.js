import React from 'react';
import {View, Text} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';


function CardManage({navigation}) {
    return(
        <View style={{backgroundColor: 'lightblue', flexDirection: 'row', justifyContent: 'space-around', height: 70, alignItems: 'center'}}>
            <Icon name='chevron-left' style={{color: 'white', fontWeight: 'bold', fontSize: 20}}/>
            <Text style={{color: 'white', fontWeight: 'bold', fontSize: 20}}>Quản lý thẻ thanh toán</Text>
            <Text/>
        </View>
    )
}

export default CardManage;
