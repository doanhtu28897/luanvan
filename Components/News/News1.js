import React from 'react';
import {View, Text, Image, ScrollView} from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome";

function News1({navigation}) {
    return(
        <View style={{backgroundColor: 'lightblue', height: '100%'}}>
            <View style={{backgroundColor: 'blue', height: '7%'}}>
                <Icon name='arrow-left' style={{color: 'white', fontWeight: 'bold',fontSize: 20, margin: 15}} onPress={() => navigation.goBack()}/>
            </View>
            <Image source={{uri: 'https://cdn.24h.com.vn/upload/4-2020/images/2020-10-06/Juventus-pha-ket-no-tin-chuyen-nhuong-610-juventus-co-chiesa-01-1601995369-744-width660height461.jpg'}} style={{width: '100%', height: '40%'}}/>
            <ScrollView>
                <Text style={{margin: 10, fontSize: 15, fontWeight: 'bold'}}>Đế chế Ronaldo ở Juventus tiến gần ngày tàn: “Bom tấn” Chiesa hay ra sao?</Text>
                <Text style={{margin: 10, fontWeight: 'bold'}}>Juventus gây sốc với quyết định chiêu mộ Federico Chiesa từ Fiorentina trong ngày cuối kỳ chuyển nhượng mùa hè 2020. Người lo lắng hơn cả với bản hợp đồng mới này, đó chính là siêu sao Ronaldo.</Text>
                <Text style={{margin: 10}}>Trong những thời khắc cuối cùng của kỳ chuyển nhượng mùa hè, Juventus quyết định chơi lớn. "Bà đầm già" quyết định chiêu mộ Federico Chiesa từ Fiorentina theo dạng cho mượn trong 2 năm với mức phí 10 triệu euro.
                    Và nếu Chiesa đáp ứng được các yêu cầu nhất định, Juventus buộc phải mua đứt cầu thủ này với giá 40 triệu euro.</Text>
                <Text style={{margin: 10}}>Trong số các yếu tố ràng buộc khiến Juventus phải bỏ tiền để mua đứt Chiesa, có việc chân sút này phải ra sân ít nhất 60% số trận, mỗi lần ra sân ít nhất 30 phút và bản thân tiền đạo này phải ghi ít nhất 10 bàn và thực hiện 10 pha kiến tạo.</Text>
                <Text style={{margin: 10}}>Hai mùa gần nhất, ở tuổi 21 và 22, Chiesa đều có nhiều hơn 10 bàn thắng mỗi mùa cho Fiorentina. Mùa 2019/20, con trai của huyền thoại Enrico Chiesa ghi 10 bàn ở Serie A cùng 6 đường kiến tạo, 11 bàn trên mọi đấu trường. 1 năm trước, Chiesa có 12 bàn cho Fiorentina.</Text>







            </ScrollView>
        </View>
    )
}

export default News1;
