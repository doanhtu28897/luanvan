import React from  'react';
import {View, Text, ScrollView, Image, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
function MainNews({navigation}) {
    return(
        <View style={{backgroundColor: 'lightblue', height: '100%'}}>
            <View style={{height: '10%', backgroundColor: 'blue', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', padding: 10}}>
                <Icon name='arrow-left' onPress={() => navigation.goBack()} style={{fontWeight: 'bold', color: 'white', fontSize: 20}}/>
                <Text style={{fontWeight: 'bold', color: 'white', fontSize: 20}}>Bảng Tin</Text>
                <Text/>
            </View>
            <ScrollView>
                <View style={{backgroundColor: 'white', margin: 10, borderRadius: 10, flexDirection: 'row'}}>
                    <Image source={{uri: 'https://cdn.24h.com.vn/upload/4-2020/images/2020-10-06/medium/ipiccy-collage-1601995448-269-width640height480.jpg'}} style={{height: 100, width: 100, borderRadius: 10, margin: 10}}/>
                    <View style={{margin: 10, justifyContent: 'space-between'}}>
                        <Text style={{width: '50%', fontSize: 15, fontWeight: 'bold'}}>Đế chế Ronaldo ở Juventus tiến gần ngày tàn: “Bom tấn” Chiesa hay ra sao?</Text>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{color: 'blue'}}  onPress={() => navigation.navigate('News1')}>Xem chi tiết</Text>
                            <Icon name="arrow-right" style={{color: 'blue', margin: 3}}/>
                        </View>
                    </View>
                </View>
                <View style={{backgroundColor: 'white', margin: 10, borderRadius: 10, flexDirection: 'row'}}>
                    <Image source={{uri: 'https://cdn.24h.com.vn/upload/4-2020/images/2020-10-07/medium/7-640-1602045672-991-width640height480.jpg'}} style={{height: 100, width: 100, borderRadius: 10, margin: 10}}/>
                    <View style={{margin: 10, justifyContent: 'space-between'}}>
                        <Text style={{width: '50%', fontSize: 15, fontWeight: 'bold'}}>Barcelona đón tin dữ: Thua lỗ quá nặng, Messi có quyền ra đi ngay lập tức?</Text>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{color: 'blue'}}  onPress={() => navigation.navigate('News2')}>Xem chi tiết</Text>
                            <Icon name="arrow-right" style={{color: 'blue', margin: 3}}/>
                        </View>
                    </View>
                </View>
                <View style={{backgroundColor: 'white', margin: 10, borderRadius: 10, flexDirection: 'row'}}>
                    <Image source={{uri: 'https://cdn.24h.com.vn/upload/4-2020/images/2020-10-07/120775239_3346754632046842_21677231894216866_n-1602040498-472-width660height464.jpg'}} style={{height: 100, width: 100, borderRadius: 10, margin: 10}}/>
                    <View style={{margin: 10, justifyContent: 'space-between'}}>
                        <Text style={{width: '50%', fontSize: 15, fontWeight: 'bold'}}>Đội Hà Nam bỏ cuộc phản đối trọng tài gây sốc, HLV Cường bị VFF cấm 5 năm</Text>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{color: 'blue'}}  onPress={() => navigation.navigate('News3')}>Xem chi tiết</Text>
                            <Icon name="arrow-right" style={{color: 'blue', margin: 3}}/>
                        </View>
                    </View>
                </View>
                <View style={{backgroundColor: 'white', margin: 10, borderRadius: 10, flexDirection: 'row'}}>
                    <Image source={{uri: 'https://cdn.24h.com.vn/upload/4-2020/images/2020-10-07/1-660-1602032829-132-width660height440.jpg'}} style={{height: 100, width: 100, borderRadius: 10, margin: 10}}/>
                    <View style={{margin: 10, justifyContent: 'space-between'}}>
                        <Text style={{width: '50%', fontSize: 15, fontWeight: 'bold'}}>Maguire ngăn Rashford khiếu nại vụ Martial thẻ đỏ: Fan đòi tước băng thủ quân MU</Text>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{color: 'blue'}}  onPress={() => navigation.navigate('News4')}>Xem chi tiết</Text>
                            <Icon name="arrow-right" style={{color: 'blue', margin: 3}}/>
                        </View>
                    </View>
                </View>
                <View style={{backgroundColor: 'white', margin: 10, borderRadius: 10, flexDirection: 'row'}}>
                    <Image source={{uri: 'https://cdn.24h.com.vn/upload/4-2020/images/2020-10-06/unnamed-660-1601917439-864-width660height440.jpg'}} style={{height: 100, width: 100, borderRadius: 10, margin: 10}}/>
                    <View style={{margin: 10, justifyContent: 'space-between'}}>
                        <Text style={{width: '50%', fontSize: 15, fontWeight: 'bold'}}>Kane & Son Heung Min phong độ hủy diệt: Tottenham thêm Bale là đủ vô địch?</Text>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{color: 'blue'}} onPress={() => navigation.navigate('News5')}>Xem chi tiết</Text>
                            <Icon name="arrow-right" style={{color: 'blue', margin: 3}}/>
                        </View>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
}

export default MainNews;
