import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

function Notification({navigation}) {
    return(
        <View style={styles.container}>
            <Text style={styles.text}>Thông báo của bạn</Text>
        </View>
    )
}

export default Notification;

const styles = StyleSheet.create({
    container:{
        justifyContent: 'center',
        height: 70,
        backgroundColor: 'lightblue',
        alignItems: 'center'
    },
    text:{
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold'
    }
})
