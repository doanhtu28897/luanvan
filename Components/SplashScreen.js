import React from 'react';
import {View} from 'react-native';
import * as Animatable from 'react-native-animatable'

function SplashScreen() {
    return(
        <View style={{height: '100%', backgroundColor: 'lightblue'}}>
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                {/*<Image source={{uri: 'https://i.pinimg.com/originals/1c/b5/d6/1cb5d6bed0d048db2d8f0a92097b0e9b.png'}} style={{width: 100, height: 100}}/>*/}
                <Animatable.Image
                    source={{uri: 'https://i.pinimg.com/originals/1c/b5/d6/1cb5d6bed0d048db2d8f0a92097b0e9b.png'}}
                    style={{width: 100, height: 100}}
                    animation="bounceIn"/>
            </View>
        </View>
    )
}
export default SplashScreen;
